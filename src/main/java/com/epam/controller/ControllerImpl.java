package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.Model;

public class ControllerImpl implements Controller {
    private Model model;
    public ControllerImpl() {
        model = new BusinessLogic();
    }
    public String getField() {
        return model.getStringField();
    }
    public void makeStrike(double power, double angle) {
        model.strike(power, angle);
    }
    public boolean strikeUpdate() {
        model.strikeUpdate();
        return model.isStrike();
    }
}
