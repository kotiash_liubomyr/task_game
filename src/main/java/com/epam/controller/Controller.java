package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.Model;

public interface Controller {
  String getField();
  void makeStrike(double power, double angle);
  boolean strikeUpdate();
}
