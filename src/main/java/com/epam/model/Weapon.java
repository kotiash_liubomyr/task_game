package com.epam.model;

public class Weapon {
    private char symbol;
    private double x;
    private double y;
    private double vx;
    private double vy;
    private double g;
    public Weapon(char symbol, double x, double y, double g) {
        this.symbol = symbol;
        this.x = x;
        this.y = y;
        this.g = g;
    }
    public void setSpeed(double V, double angle) {

    }
    public void updatePosition() {

    }

    public char getSymbol() {
        return symbol;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
}
