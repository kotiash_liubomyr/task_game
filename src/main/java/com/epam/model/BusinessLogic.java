package com.epam.model;

public class BusinessLogic implements Model {
    private final double G = 0.000235848901098901;
    private final double K = 0.00167428571428571;
    private FieldOfPlay field;
    private Player player1;
    private Player player2;
    private Weapon fireBoll;
    private boolean isMovePlayer1 = true;
    private boolean isStrike;

    public BusinessLogic() {
        field = new FieldOfPlay("FieldOfPlay.txt");
        player1 = new Player('b', 10, 16);
        player2 = new Player('d', 101, 16);
        fireBoll = new Weapon('*', 0, 0, G);
        isStrike = false;
    }

    /*Буде задавати початок вистрілу*/
    public void strike(double power, double angle) {
        double speed = power * K;
        if (isMovePlayer1) {
            fireBoll.setX(player1.getX() + 1);
            fireBoll.setY(player1.getY() - 1);
            fireBoll.setSpeed(speed, angle);
        } else {
            fireBoll.setX(player2.getX() - 1);
            fireBoll.setY(player2.getY() - 1);
            fireBoll.setSpeed(-speed, angle);
        }
        isStrike = true;
    }

    /*Оновлення координат після вистрілу*/
    public void strikeUpdate() {
        fireBoll.updatePosition();
        if (field.isGround(fireBoll)) {
            isStrike = false;
            isMovePlayer1 = !isMovePlayer1;
        }
    }

    /*повертає поле в форматі для виводу(String) */
    public String getStringField() {
        if (isStrike) {
            return field.replaysWithWeapon(player1, player2, fireBoll);
        } else {
            return field.replaysWithoutWeapon(player1, player2);
        }
    }

    /*перевірка чи закінчився постріл*/
    public boolean isStrike() {
        return isStrike;
    }
}
