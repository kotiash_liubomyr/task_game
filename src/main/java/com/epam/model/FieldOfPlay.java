package com.epam.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FieldOfPlay {
    private StringArray field;

    /* мало б зчитувати карту з файлу FieldOfPlay.txt i записувати в field*/
    FieldOfPlay(String path) {
        int i = 0;
        try (BufferedReader br = new BufferedReader(new FileReader("*\\src\\FieldOfPlay.txt"))) {
            String s;
            while ((s = br.readLine()) != null) {
                i++;
                field.setSting(i, s);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /*Якщо координати зброї(снаряду) співпадає з землею(символи '_' і '|', і край карти '▒') повертає true */
    public boolean isGround(Weapon weapon) {//28/119
        char sym = field.at((int) Math.round(weapon.getX()), (int) Math.round(weapon.getY()));
        if (sym == '_' || sym == '|' || sym == '▒') {
            return true;
        }
        return false;
    }

    /*повертає поле в форматі стрінг для виоду на екран*/
    // без зброї на екрані
    public String replaysWithoutWeapon(Player player1, Player player2) {
        String string = new String("");
        StringArray temp = field.clone();
        temp.set(player1.getX(), player2.getY(), 'b');
        temp.set(player2.getX(), player2.getY(), 'd');
        string = temp.toString();
        return string;
    }

    // з зброєю на екрані
    public String replaysWithWeapon(Player player1, Player player2, Weapon weapon) {
        String string = new String("");
        StringArray temp = field.clone();
        temp.set(player1.getX(), player2.getY(), 'b');
        temp.set(player2.getX(), player2.getY(), 'd');
        temp.set((int) Math.round(weapon.getX()), (int) Math.round(weapon.getY()), '*');
        string = temp.toString();
        return string;
    }

}
