package com.epam.model;

public class StringArray {
    private String[] array;
    private int arrayWeight;
    private int arrayHeight;

    public StringArray(int weight, int height) {
        array = new String[height];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < weight; j++) {
                array[i] += '\0';
            }
        }
        arrayWeight = weight;
        arrayHeight = height;
    }
    public StringArray(StringArray stringArray) {
        arrayHeight = stringArray.arrayHeight;
        arrayWeight = stringArray.arrayWeight;
        array = new String[arrayHeight];
        for(int i =0; i < arrayHeight; i++) {
            array[i] = stringArray.array[i];
        }
    }
    public void setSting(int i, String str) {
        array[i] = str;
    }
    public String getString(int i) {
        return array[i];
    }
    public char at(int x, int y) {
        return array[y].charAt(x);
    }
    public void set(int x, int y, char c) {
        char[] temp = array[y].toCharArray();
        temp[x] = c;
        array[y] = new String(temp);
    }
    public StringArray clone() {
        return new StringArray(this);
    }
    public String toString() {
        String result = array[0] + "\n";
        for (int i = 1; i < array.length; i++) {
            result += array[i];
            if (i != array.length - 1) {
                result += "\n";
            }
        }
        return result;
    }
}
