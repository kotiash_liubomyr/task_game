package com.epam.model;

public interface Model {
    public String getStringField();
    public void strike(double power, double angle);
    public void strikeUpdate();
    public boolean isStrike();
}
