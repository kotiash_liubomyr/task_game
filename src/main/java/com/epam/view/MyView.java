package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("HELP", "  HELP - print help menu");
        menu.put("PLAY", "  PLAY - start game");
        menu.put("STRIKE", "  STRIKE - to make strike");
        menu.put("QUIT", "  QUIT - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("HELP", this::outputMenu);
        methodsMenu.put("PLAY", this::startGame);
        methodsMenu.put("STRIKE", this::strike);
    }

    private void outputMenu() {
        for(int i = 0; i < 25; i++) {
            System.out.println();
        }
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
    /* Вигружає на екран початкову карту*/
    private void startGame() {
        System.out.println(controller.getField());
    }
    /*Зчитує параметри вистрілу і промальовує анімацію предаючи параметри на крнтролер*/
    private void strike() {

    }
    public void show() {
        outputMenu();
        String command;
        do {
            System.out.println("Please, select menu point.");
            command = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(command).print();
            } catch (Exception e) {
            }
        } while (!command.equals("QUIT"));
    }
}